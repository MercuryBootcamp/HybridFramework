package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.LoginPage;

public class TC052_DeleteWorkTypeGroup extends ProjectSpecificMethods {

	@BeforeTest
	public void setValues() {
		testcaseName = "Delete Work Type Group";
		testDescription = "Verify Delete Work Type Group";
		authors = "Ram";
		category = "Functional";
		excelFileName = "DeleteWorkTypeGroup";
	}

	@Test(dataProvider = "fetchData")
	public void editWorkTypeGroup(String username, String password, String workTypeGroupName) {

		new LoginPage().enterUsername(username).enterPassword(password).clickLogin().clickGlobalAction().clickViewAll()
				.clickWorkTypeGroups().searchWorkTypeGroups(workTypeGroupName).clickMoreActions()
				.clickDeleteWorkTypeGroup().clickYesDeleteConfirmation().verifyAfterWorkTypeDelete(workTypeGroupName);
	}

}
