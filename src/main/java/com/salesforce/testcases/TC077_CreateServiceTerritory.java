
package com.salesforce.testcases;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.LoginPage;


public class TC077_CreateServiceTerritory extends ProjectSpecificMethods {
	@BeforeTest
	
		public void setValues() {
			testcaseName = "CreateServiceTerritory";
			testDescription ="New Service Territory Page is created";
			authors="Anitha";
			category ="Smoke";
			excelFileName="ServiceExcel";
		}

	@Test (dataProvider="fetchData")
		public void runLogin(String username, String password, String appName, String appNameSubstring, String Name, String City, String State, String Country, String zipcode) throws InterruptedException, Exception {

			new LoginPage().enterUsername(username).enterPassword(password).clickLogin().clickGlobalAction().clickViewAll().clickServiceTerritories(appName, appNameSubstring).clickNew().enterName(Name).clickOpertaingHours().checkActiveField().enterCity(City).enterState(State).enterCountry(Country).enterPostalZip(zipcode).clickSave().takeScreenshot();		

		}
	}

