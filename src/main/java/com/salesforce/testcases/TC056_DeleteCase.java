package com.salesforce.testcases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.LoginPage;

public class TC056_DeleteCase extends com.framework.testng.api.base.ProjectSpecificMethods {

		@BeforeTest
		public void setValues() {
			testcaseName = "DeleteCase";
			testDescription = "Verify Delete case with positive data";
			authors = "Shalini";
			category = "Smoke";
			excelFileName="DeleteCase";
		}

		@Test(dataProvider = "fetchData")
		public void runLogin(String username, String password, String subject) throws InterruptedException {
			new LoginPage().enterUsername(username).enterPassword(password).clickLogin()
			.clickGlobalAction().clickViewAll()
			.clickSales().clickMoreOptions().ClickSearchCase(subject).clickDelete().verifyAfterDeletingCase();
	}


}
