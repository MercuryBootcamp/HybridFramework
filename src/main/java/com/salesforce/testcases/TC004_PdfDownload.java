package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;

import com.salesforce.pages.LoginPage;
import com.salesforce.pages.ReleaseNotesPage;

public class TC004_PdfDownload extends ProjectSpecificMethods {
	@BeforeTest
	public void setValues() {
		testcaseName = "PDF download";
		testDescription = "Download the pdf and verify itg";
		authors = "Veera";
		category = "Smoke";
		excelFileName = "Login";
	}

	@Test(dataProvider = "fetchData")
	public void runPDFDownload(String username, String pwd) {
		LoginPage lp = new LoginPage();
		lp.enterUsername(username).enterPassword(pwd).clickLogin().clickViewReleaseNotes().clickConfirm();
		ReleaseNotesPage rnp = new ReleaseNotesPage();
		rnp.selectReleaseVersion().clickPDF().verifyUrl();

	}
}
