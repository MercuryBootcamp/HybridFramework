package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.CampaignCreationPage;
import com.salesforce.pages.CampaignCreationPage;
import com.salesforce.pages.LoginPage;

public class TC088_CreateCampaign extends ProjectSpecificMethods {

	@BeforeTest
	public void setValues() {
		testcaseName = "Create Campaign";
		testDescription ="Verify Campagin creation with positive data";
		authors="Sarath";
		category ="Smoke";
		excelFileName="CampaignCreation";
	}

	@Test(dataProvider = "fetchData")
	public void runLogin(String username, String password,String cmpName) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.verifyHomePage()
		.clickGlobalAction()
		.clickViewAll()
		.clickSales();
		new CampaignCreationPage()
		.ClickNewCampaign()
		.EnterCampaignName(cmpName)
		.EnterStartDateTomm()
		.EnterEndDateTommPlusOne()
		.ClickSave();

	}

}
