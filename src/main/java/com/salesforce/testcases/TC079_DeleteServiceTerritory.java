package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.DeleteServiceTerritoryPage;
import com.salesforce.pages.LoginPage;


public class TC079_DeleteServiceTerritory extends ProjectSpecificMethods {

		@BeforeTest
		public void setValues() {
			testcaseName = "CreateServiceTerritory";
			testDescription ="New Service Territory Page is created";
			authors="Anitha";
			category ="Smoke";
			excelFileName="DeleteServiceTerritory";
		}
		
		@Test (dataProvider="fetchData")
		public void runLogin(String username, String password, String appName, String appNameSubstring, String Name) throws InterruptedException{
			new LoginPage()
			.enterUsername(username)
			.enterPassword(password).clickLogin()
			.clickGlobalAction()
			.clickViewAll()
			.clickServiceTerritories(appName, appNameSubstring);
			
			new DeleteServiceTerritoryPage()
			.searchEntity(Name)
			.deleteEntity()
			.verifyDeleteSuccessfull();
		}
}
