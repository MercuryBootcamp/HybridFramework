package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.LoginPage;

public class TC051_EditWorkTypeGroup extends ProjectSpecificMethods {

	@BeforeTest
	public void setValues() {
		testcaseName = "Edit Work Type Group";
		testDescription = "Verify Edit Work Type Group";
		authors = "Ram";
		category = "Functional";
		excelFileName = "EditWorkTypeGroup";
	}

	@Test(dataProvider = "fetchData")
	public void editWorkTypeGroup(String username, String password, String workTypeGroupName, String description,
			String groupType) {

		new LoginPage().enterUsername(username).enterPassword(password).clickLogin().clickGlobalAction().clickViewAll()
				.clickWorkTypeGroups().searchWorkTypeGroups(workTypeGroupName).clickMoreActions()
				.clickEditWorkTypeGroup().enterDescription(description).enterGroupType(groupType).clickSave()
				.searchWorkTypeGroups(workTypeGroupName).clickWorkTypeGroup(workTypeGroupName)
				.verifyDescription(description);

	}

}
