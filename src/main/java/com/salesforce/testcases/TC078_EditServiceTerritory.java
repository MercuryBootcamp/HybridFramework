package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.EditServiceTerritoryPage;
import com.salesforce.pages.LoginPage;

public class TC078_EditServiceTerritory extends ProjectSpecificMethods {
	@BeforeTest

	public void setValues() {
		testcaseName = "EditServiceTerritory";
		testDescription ="Edit Service Territory Page and update Country as North America";
		authors="Anitha";
		category ="Smoke";
		excelFileName="EditServiceTerritory";
	}

	@Test (dataProvider="fetchData")
	public void runLogin(String username, String password, String appName, String appNameSubstring, String Name, String textCreatedBy, String textLastModifiedBy) throws InterruptedException, Exception {

		new LoginPage()
		.enterUsername(username)
		.enterPassword(password).clickLogin()
		.clickGlobalAction()
		.clickViewAll()
		.clickServiceTerritories(appName, appNameSubstring);


		EditServiceTerritoryPage ep=new EditServiceTerritoryPage();
		String createdBy = ep.selectRow(Name).clickDropDownArrow().clickEdit().getCreatedBy();
		String modifiedBy = ep.getModifiedBy();
		ep.getOwnerDetails();
		ep.verifyModifiedDetails(createdBy, modifiedBy);
		ep.changeCountry().clickSaveAfterEdit().verifyLastModifiedBy();

	}
}

