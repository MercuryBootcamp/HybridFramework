package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.LoginPage;

public class TC055_EditCase extends ProjectSpecificMethods{

	@BeforeTest
	public void setValues() {
		testcaseName = "EditCase";
		testDescription = "Verify Edit case with positive data";
		authors = "Ram";
		category = "Smoke";
		excelFileName = "EditCase";		
	}
	@Test(dataProvider="fetchData")
	public void editCase(String username, String password, String status, String caseOrigin, String priority, String SLAValue) throws InterruptedException {		
		new LoginPage().enterUsername(username).enterPassword(password).clickLogin().clickGlobalAction().clickViewAll()
		.clickSales().clickMoreOptions().clickCasesoption().click3MoreActions().clickEdit().setStatus(status).setCaseOrigin(caseOrigin).
		setPriority(priority).setSLAViolation(SLAValue).clickSaveButton().verifyStatusAfterEditing(status);
		
	}
}
