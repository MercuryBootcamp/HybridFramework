package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.LoginPage;

public class TC129_CreatePaymentWithoutMandatoryfield extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setValues() {
		testcaseName = "Creating Payment WithOut Mandatory field";
		testDescription ="Creating Payment WithOut Mandatory field to verify Error Message";
		authors="Sarathkumar";
		category ="Smoke";
		excelFileName="Login";
	}

	@Test(dataProvider = "fetchData")
	public void runLogin(String username, String password) throws InterruptedException {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.verifyHomePage()
		.clickGlobalAction()
		.clickViewAll()
		.clickPaymentAuthorization()
		.ClickNewPayment()
		.ClickSave()
		.VerifyErrorMessage();
	}
}
