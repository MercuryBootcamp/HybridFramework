package com.salesforce.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.LoginPage;
import com.salesforce.pages.ServiceTerritoriesPage;

public class TC002_CreateTerritories extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setValues() {
		
		testcaseName = "CreateTerritories";
		testDescription ="Verify Create Territories functionality with positive data";
		authors="Venki";
		category ="Smoke";
		excelFileName="ServiceExcel";
	}
	
	@Test (dataProvider="fetchData")
	public void runLogin(String username, String password, String appName, String appNameSubstring, String Name, String City, String State, String Country, String zipcode) throws InterruptedException, Exception {

		new LoginPage().enterUsername(username).enterPassword(password).clickLogin().clickGlobalAction().clickViewAll().clickServiceTerritories(appName, appNameSubstring).clickNew().enterName(Name).clickOpertaingHours().checkActiveField().enterCity(City).enterState(State).enterCountry(Country).enterPostalZip(zipcode).clickSave().takeScreenshot();		

	}
	
}



