package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.LoginPage;
import com.salesforce.pages.OppurtunityCreation;

public class TC025_CreateOppurtunities extends ProjectSpecificMethods {
	
	
	@BeforeTest
	public void setValues() {
		testcaseName = "Create Campaign";
		testDescription ="Verify CreateOppurtunities with positive data's";
		authors="Sarathkumar";
		category ="Smoke";
		excelFileName="Login";
	}

	@Test(dataProvider = "fetchData")
	public void runLogin(String username, String password) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.verifyHomePage()
		.clickGlobalAction()
		.clickViewAll()
		.clickSales();
		
		new OppurtunityCreation()
		.ClickOppurtunityTab()
		.ClickNewOppurtunity()
		.EnterOpportunityName()
		.ChooseCloseDate()
		.SelectStage()
		.SaveOppurtunity()
		.VerifyOppurtunity();
		
	}

}
