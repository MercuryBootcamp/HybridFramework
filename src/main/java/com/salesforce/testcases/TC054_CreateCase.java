package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.LoginPage;

public class TC054_CreateCase extends ProjectSpecificMethods {

	@BeforeTest
	public void setValues() {
		testcaseName = "CreateCase";
		testDescription = "Verify Create case with positive data";
		authors = "Ram";
		category = "Smoke";
		excelFileName = "CreateCase";
	}

	@Test(dataProvider = "fetchData")
	public void runLogin(String username, String password, String ctName, String status, String caseOrigin, String desc,
			String subject) {
		new LoginPage().enterUsername(username).enterPassword(password).clickLogin().clickGlobalAction().clickViewAll()
				.clickSales().clickMoreOptions().clickCasesoption().clickNew().setStatus(status).setCaseOrigin(caseOrigin)
				.chooseContacts(ctName).setSubject(subject).setDescription(desc).clickSaveButton()
				.verifyAfterCreatingCase();

	}

}
