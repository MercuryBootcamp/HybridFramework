package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;
import com.salesforce.pages.LoginPage;

public class TC050_CreateWorkTypeGroup extends ProjectSpecificMethods {

	@BeforeTest
	public void setValues() {
		testcaseName = "Verify Create Work Type Group";
		testDescription = "Verify Create Work Type Group with positive data";
		authors = "Ram";
		category = "Smoke";
		excelFileName = "CreateWorkTypeGroup";
	}

	@Test(dataProvider = "fetchData")
	public void group(String username, String password, String workTypeGroupName) {
		LoginPage login = new LoginPage();

		login.enterUsername(username).enterPassword(password).clickLogin().clickGlobalAction().clickViewAll()
				.clickWorkTypeGroups().clickWorkTypeGroupsDropDown().clickNewWorkTypeGroups()
				.enterWorkTypeGroupName(workTypeGroupName).clickSave().verifyCreatedWorkTypeGroup(workTypeGroupName);

	}

}
