package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.testng.api.base.ProjectSpecificMethods;

import com.salesforce.pages.HomePage;
import com.salesforce.pages.LoginPage;

public class TC002_CreateLeads extends ProjectSpecificMethods {

	@BeforeTest
	public void setValues() {
		testcaseName = "VerifyLogin";
		testDescription = "Verify Login functionality with positive data";
		authors = "Veera";
		category = "Smoke";
		excelFileName = "CreateLead";
}

	@Test(dataProvider = "fetchData")
	public void runCreateLeads(String uname, String pwd, String fname, String company) {

		LoginPage lp = new LoginPage();
		lp.enterUsername(uname).enterPassword(pwd).clickLogin();
		HomePage hp = new HomePage();
		hp.clickGlobalAction().clickViewAll().clickSales().clickLeads().clickNewButton().enterFirstName(fname)
				.enterCompanyName(company).clickSave().verifyMessage();

	}

}
