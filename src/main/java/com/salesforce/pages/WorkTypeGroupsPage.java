package com.salesforce.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class WorkTypeGroupsPage extends ProjectSpecificMethods {

	public WorkTypeGroupsPage searchWorkTypeGroups(String workTypeGroupName) {
		typeAndEnter(locateElement(Locators.XPATH, "//input[@aria-label='Search Recently Viewed list view.']"),
				workTypeGroupName);
		getDriver().findElement(By.xpath("//input[@aria-label='Search Recently Viewed list view.']"))
				.sendKeys(Keys.ENTER);
		reportStep("Searching for the WorkType Group Name", "pass");
		return this;
	}

	public WorkTypeGroupsPage clickMoreActions() {
		click(Locators.XPATH, "//span[text()='Show Actions']/parent::span");
		reportStep("More Actions Button is clicked", "pass");
		return this;
	}

	public WorkTypeGroupsPage clickWorkTypeGroupsDropDown() {
		click(Locators.XPATH,
				"(//a[contains(@title,'Work Type Groups')]/following::one-app-nav-bar-item-dropdown//lightning-icon/lightning-primitive-icon)[2]");
		reportStep("WorkType Groups Dropdown is clicked", "pass");
		return this;
	}

	public CreateWorkTypeGroupsPage clickNewWorkTypeGroups() {
		clickUsingJs(locateElement(Locators.XPATH, "//span[text()='New Work Type Group']"));
		reportStep("New WorkType Groups button is clicked", "pass");
		return new CreateWorkTypeGroupsPage();
	}

	public CreateWorkTypeGroupsPage clickEditWorkTypeGroup() {
		click(Locators.XPATH, "//a[@title='Edit']");
		reportStep("Edit button is clicked", "pass");
		return new CreateWorkTypeGroupsPage();
	}

	public WorkTypeGroupsPage clickDeleteWorkTypeGroup() {
		click(Locators.XPATH, "//a[@title='Delete']");
		reportStep("Delete button is clicked", "pass");
		return new WorkTypeGroupsPage();
	}

	public WorkTypeGroupsPage clickYesDeleteConfirmation() {
		click(Locators.XPATH, "//span[text()='Delete']");
		reportStep("Edit button is clicked", "pass");
		return this;
	}

	public WorkTypeGroupsPage clickWorkTypeGroup(String workTypeGroupName) {
		click(Locators.XPATH, "//a[text()='" + workTypeGroupName + "']");
		reportStep("Work Type Group is clicked", "pass");
		return this;
	}

	public WorkTypeGroupsPage verifyDescription(String description) {
		verifyExactText(locateElement(Locators.XPATH, "//span[text()='" + description + "']"), description);
		reportStep("Getting the text of Description and Verifying", "pass");
		return this;
	}

	public WorkTypeGroupsPage verifyAfterWorkTypeDelete(String workTypeGroupName) {
		try {
			Thread.sleep(1000);
			searchWorkTypeGroups(workTypeGroupName);
			verifyDisappeared(locateElement(Locators.XPATH, "//tbody/tr"));
			reportStep("Delete Verification is done", "pass");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public WorkTypeGroupsPage verifyCreatedWorkTypeGroup(String workTypeGroupName) {
		verifyExactAttribute(locateElement(Locators.XPATH, "//div[@title='"+workTypeGroupName+"']"), "title", workTypeGroupName);
		reportStep("Name for WorkType Group is entered", "pass");
		return this;
	} 

}
