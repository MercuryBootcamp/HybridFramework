package com.salesforce.pages;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class CreateWorkTypeGroupsPage extends ProjectSpecificMethods {
	
	public CreateWorkTypeGroupsPage enterWorkTypeGroupName(String workTypeGroupName) {
		clearAndType(locateElement(Locators.XPATH, "//input[@type='text']"), workTypeGroupName);
		reportStep("Name for WorkType Group is entered", "pass");
		return this;
	}
	
	public WorkTypeGroupsPage clickSave() {
		click(Locators.XPATH, "//button[@title='Save']");
		reportStep("Name for WorkType Group is entered", "pass");
		return new WorkTypeGroupsPage();
	} 
	
	public CreateWorkTypeGroupsPage enterDescription(String description) {
		clearAndType(locateElement(Locators.XPATH, "//textarea"),description);
		reportStep("Edit button is clicked", "pass");
		return this;		
	}

	public CreateWorkTypeGroupsPage enterGroupType(String groupType) {
		click(Locators.XPATH, "//span[text()='Group Type']/parent::span/following::a");
		click(Locators.XPATH, "//a[@title='"+groupType+"']");
		reportStep("Group Type filter is set", "pass");
		return this;
	}
	
		
}
