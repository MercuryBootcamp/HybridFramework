package com.salesforce.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class OppurtunityCreation extends ProjectSpecificMethods {


	public OppurtunityCreation ClickOppurtunityTab() {	
		
		WebElement element= (locateElement(Locators.XPATH, "//span[@class='slds-truncate'][text()='Opportunities']"));
		JavascriptExecutor executor =(JavascriptExecutor)getDriver();
		executor.executeScript("arguments[0].click();",element);
		reportStep("Click on OppurtunityTab", "pass");
		return this;		

	}
	
	public OppurtunityCreation ClickNewOppurtunity() {	

		click(locateElement(Locators.XPATH, "//div[@title='New']"));
		reportStep("Click on NewOppurtunity", "pass");
		return this;

	}

	public OppurtunityCreation EnterOpportunityName() {	

		clearAndType(locateElement(Locators.XPATH,"(//input[@class='slds-input'])[4]"),"Salesforce Automation by Sarath");
		reportStep("Oppurtunity Name Entered", "pass");
		return this;

	}

	public OppurtunityCreation ChooseCloseDate() {	

		clearAndType(locateElement(Locators.XPATH,"//input[@name='CloseDate']"),"8/13/2022");
		reportStep("CloseDate has been selected", "pass");
		return this;

	}
	
	public OppurtunityCreation SelectStage() {	

		click(locateElement(Locators.XPATH, "(//label[text()='Stage']/following::div/button)[1]"));
		click(locateElement(Locators.XPATH, "(//label[text()='Stage']/following::div/button)[1]"));
		click(locateElement(Locators.XPATH, "//div/lightning-base-combobox-item/span/span[@title='Needs Analysis']"));
		reportStep("Selected 'Stage' as Need Analysis", "pass");
		return this;

	}
	
	public OppurtunityCreation SaveOppurtunity() {	

		click(locateElement(Locators.XPATH, "//button[@name='SaveEdit']"));
		reportStep("Oppurtunity Created Saved", "pass");
		return this;


	}

	public OppurtunityCreation VerifyOppurtunity() {

		String txt2=getElementText(locateElement(Locators.XPATH,"//span[@data-aura-class='forceActionsText']"));
		System.out.println(txt2);
		reportStep("Oppurtunity Created Successfully", "pass");
		return this;
	}

}
