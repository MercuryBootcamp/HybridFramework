package com.salesforce.pages;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class DeleteServiceTerritoryPage extends ProjectSpecificMethods {
	
	public DeleteServiceTerritoryPage searchEntity(String Name) {
		type(locateElement(Locators.XPATH, "//input[@placeholder='Search this list...']"), Name);
		return this;
	}
	
	public DeleteServiceTerritoryPage deleteEntity() throws InterruptedException{
		Thread.sleep(3000);
		click(locateElement(Locators.XPATH, "(//div[contains(@class,'forceVirtualAction')])[1]"));
		Thread.sleep(3000);
		click(locateElement(Locators.XPATH, "//a[@title='Delete']"));
		Thread.sleep(3000);
		click(locateElement(Locators.XPATH, "//span[text()='Delete']")); 
		return this;
	}

	public DeleteServiceTerritoryPage verifyDeleteSuccessfull() {
		String text = getElementText(locateElement(Locators.XPATH,"//span[text()='Service Territory \"']"));
		
		if(text.contains("Rupesh"))
		{
			System.out.println("Row got deleted");
		}
		
		else {
			
				System.out.println("Row NOT deleted");
			}
		return this;
		}
			
	}


