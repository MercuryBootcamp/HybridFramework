package com.salesforce.pages;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class ReleaseNotesPage extends ProjectSpecificMethods {

	public ReleaseNotesPage selectReleaseVersion() {
		click(Locators.XPATH, "//button[@name='releaseVersionPicker']/span");
		clickUsingJs(locateElement(Locators.XPATH, "(//span[@class='slds-media__body']/span)[7]"));
		reportStep("releaseVersion is selected", "pass");
		return new ReleaseNotesPage();
	}

	public PdfDocumentPage clickPDF() {
		click(Locators.XPATH, "//button[text()='PDF']");
		reportStep("Pdf downloaded", "pass");
		return new PdfDocumentPage();
	}
}
