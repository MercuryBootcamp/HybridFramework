package com.salesforce.pages;


import org.openqa.selenium.WebElement;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class NewLeadPage extends ProjectSpecificMethods {

	public NewLeadPage selectSalutaion() {

		click(locateElement(Locators.XPATH, "//button[@aria-label='Salutation, --None--']"));
		click(locateElement(Locators.XPATH, "//span[text()='Mr.']"));
		reportStep("Saluation Selected as Mr. Successfully", "pass");
		return this;
	}

	public NewLeadPage enterFirstName(String data) {

		WebElement ele = locateElement(Locators.XPATH, "//input[@name='firstName']");
		clearAndType(ele, "veeravel");
		reportStep(data + " entered successfully", "pass");
		return this;

	}

	public NewLeadPage enterCompanyName(String data) {
		WebElement ele = locateElement(Locators.XPATH, "//input[@name='Company']");
		clearAndType(ele, "HCl");
		reportStep(data + " entered successfully", "pass");
		return this;
	}

	public NewLeadPage clickSave() {
		click(locateElement(Locators.XPATH, "//button[text()='Save']"));
		reportStep("Save Button clicked  Successfully", "pass");
		return this;

	}

	public void verifyMessage() {
		String expectedText = "Complete this field";
		WebElement ele = locateElement(Locators.XPATH, "//div[text()='Complete this field.']");
		verifyExactText(ele, expectedText);
		//quit();
		reportStep("Message verified successfully", "pass");

	}


}
