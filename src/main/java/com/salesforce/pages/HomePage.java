package com.salesforce.pages;

import org.openqa.selenium.By;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class HomePage extends ProjectSpecificMethods {

	public HomePage verifyHomePage() {
		verifyDisplayed(locateElement(Locators.XPATH, "//span[text()='Home']"));
		reportStep("Homepage is loaded", "pass");
		return this;
	}

	public HomePage clickGlobalAction() {
		click(Locators.XPATH, "//div[@class='slds-icon-waffle']");
		reportStep("Global Action icon is clicked", "pass");
		return this;
	}

	public AppLauncherPage clickViewAll() {
		click(Locators.XPATH, "//button[text()='View All']");
		reportStep("Viewall button is clicked", "pass");
		return new AppLauncherPage();
	}

	public ConfirmationPage clickLearnMore() {
		click(Locators.XPATH, "//span[text()='Learn More']");
		reportStep("LearnMore button is clicked", "pass");
		return new ConfirmationPage();
	}

	public ServiceTerritoriesPage clickServiceTerritories() {

		clearAndType(locateElement(Locators.XPATH, "//input[@placeholder='Search apps or items...']"),"Service Territories");
		waitForApperance(locateElement(Locators.XPATH, "//p[@class='slds-truncate']/mark"));
		click(locateElement(Locators.XPATH, "//p[@class='slds-truncate']/mark"));
		reportStep("Clicked the Service Territories", "pass");
		return new ServiceTerritoriesPage();
	}

	public ConfirmationPage clickViewReleaseNotes() {
		click(Locators.XPATH, "//div[@class='rightScroll']/button");
		click(Locators.XPATH, "//div[@class='rightScroll']/button");
		click(Locators.XPATH, "//span[text()=': Release Notes']/preceding-sibling::span");
		return new ConfirmationPage();
	}
}