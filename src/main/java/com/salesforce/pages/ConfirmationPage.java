package com.salesforce.pages;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class ConfirmationPage extends ProjectSpecificMethods {
	
	public ProductsPage clickConfirm() {
		
		switchToWindow(1);
		click(Locators.XPATH, "//button[text()='Confirm']");
		reportStep("Confirm button is clicked", "pass");
		return new ProductsPage();

	}

}
