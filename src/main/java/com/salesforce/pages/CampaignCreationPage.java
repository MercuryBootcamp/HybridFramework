package com.salesforce.pages;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class CampaignCreationPage extends ProjectSpecificMethods {

	public LocalDate today; 

	public CampaignCreationPage ClickNewCampaign() {	
		click(locateElement(Locators.XPATH, "(//a/span[text()='Campaigns']/following::one-app-nav-bar-item-dropdown)[1]"));
		click(locateElement(Locators.XPATH, "//one-app-nav-bar-menu-item[@class='slds-dropdown__item'][1]"));
		reportStep("Click on Create NewCampaign", "pass");
		return this;

	}
	
	public CampaignCreationPage	EnterCampaignName(String cmpName) {	
		clearAndType(locateElement(Locators.XPATH,"(//label/span[text()='Campaign Name']/following::input)[1]"),cmpName);
		reportStep("CampaignName Entered", "pass");
		return this;

	}
	
	public CampaignCreationPage EnterStartDateTomm(){
		today = LocalDate.now();
		LocalDate tomorrow = today.plusDays(1);
		String tomm =tomorrow.format(DateTimeFormatter.ISO_DATE);
		clearAndType(locateElement(Locators.XPATH,"(//input[@class=' input'])[2]"),tomm);
		reportStep("StartDate Entered As Tomorrow Date", "pass");
		return this;

	}

	public CampaignCreationPage EnterEndDateTommPlusOne() {
		LocalDate today1 =LocalDate.now();
		LocalDate tomorrow1 =today.plusDays(2);
		String tomm1=tomorrow1.format(DateTimeFormatter.ISO_DATE);
		clearAndType(locateElement(Locators.XPATH,"(//input[@class=' input'])[3]"),tomm1);
		reportStep("EndDate Entered As Tomorrow +1", "pass");
		return this;
	}

	public CampaignCreationPage ClickSave() {
		click(locateElement(Locators.XPATH, "//button/span[text()='Save']"));
		reportStep("Saved Campaign Creation", "pass");
		String txt=getElementText(locateElement(Locators.XPATH,"//div[@data-key='success']/div//span"));
		System.out.println(txt);
		reportStep("Create Campaign Created Successfully", "pass");
		return this;
	}

}
