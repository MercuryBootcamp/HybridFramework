package com.salesforce.pages;


import java.io.IOException;
import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class CreateServiceTerritoryPage extends ProjectSpecificMethods{

	public CreateServiceTerritoryPage clickNew() throws InterruptedException {
		click(locateElement(Locators.XPATH, "//div[@title='New']"));
		reportStep("New Create option is clicked", "pass");
		return this;
	}

	public CreateServiceTerritoryPage enterName(String Name) {
		type(locateElement(Locators.XPATH, "//h2[text()='New Service Territory']/following::span[text()='Name']/following::input[1]"), Name);
		reportStep("Name is Entered", "pass");
		return this;	
	}


	public CreateServiceTerritoryPage clickOpertaingHours(){
		click(locateElement(Locators.XPATH, "//h2[text()='New Service Territory']/following::span[text()='Operating Hours']/following::input[1]"));
		click(locateElement(Locators.XPATH, "//div[@title='US Shift']"));
		reportStep("Operating Hours is Selected", "pass");
		return this;
	}

	public CreateServiceTerritoryPage checkActiveField(){

		click(locateElement(Locators.XPATH,"//h2[text()='New Service Territory']/following::span[text()='Active']/following::input[1]"));
		reportStep("Active Checkbox is Clicked", "pass");
		return this;
	}

	public CreateServiceTerritoryPage enterCity(String city) {
		type(locateElement(Locators.XPATH, "//h2[text()='New Service Territory']/following::span[text()='City']/following::input[1]"), city);
		reportStep("City is Entered", "pass");
		return this;
	}

	public CreateServiceTerritoryPage enterState(String State) {
		type(locateElement(Locators.XPATH, "//h2[text()='New Service Territory']/following::span[text()='State/Province']/following::input[1]"), State);
		reportStep("State is Entered", "pass");
		return this;
	}

	public CreateServiceTerritoryPage enterCountry(String Country) {
		type(locateElement(Locators.XPATH, "//h2[text()='New Service Territory']/following::span[text()='Country']/following::input[1]"), Country);
		reportStep("Country is Entered", "pass");
		return this;
	}

	public CreateServiceTerritoryPage enterPostalZip(String Zipcode) {  
		type(locateElement(Locators.XPATH, "//h2[text()='New Service Territory']/following::span[text()='Zip/Postal Code']/following::input[1]"), Zipcode);	
		reportStep("Zipcode is Entered", "pass");
		return this;
	}

	public CreateServiceTerritoryPage clickSave() throws IOException {
		click(locateElement(Locators.XPATH,"(//span[text()='Save'])[2]"));	
		reportStep("Page is Saved", "pass");
		return this;

	}
	public CreateServiceTerritoryPage takeScreenshot() throws IOException {
		takeSnap();
		//		takeSnap(locateElement(Locators.XPATH,"//span[text()='Service Territory']"));
		//		WebElement findToastEle = driver.findElement(By.xpath("//span[text()='Service Territory']"));
		//		File src = findToastEle.getScreenshotAs(OutputType.FILE);
		//		File dec = new File("./elesnaps/img.png");
		//		FileUtils.copyFile(src, dec);
		return this; 
	}




}