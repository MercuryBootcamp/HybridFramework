package com.salesforce.pages;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class AppLauncherPage extends ProjectSpecificMethods {

	public SalesPage clickSales() {
		click(locateElement(Locators.XPATH, "//p[text()='Sales']"));
		reportStep(" Sales link clicked successfully", "pass");
		return new SalesPage();
	}

	public WorkTypeGroupsPage clickWorkTypeGroups() {
		clickUsingJs(locateElement(Locators.XPATH, "//p[text()='Work Type Groups']"));
		reportStep("Work Type Group is clicked", "pass");
		return new WorkTypeGroupsPage();
	}
	
	public CreateServiceTerritoryPage clickServiceTerritories(String appName, String appNameSubstring) {
		//		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys(appName);
		//		driver.findElement(By.xpath("//p[contains(text(), '"+ appNameSubstring +"')]")).click();

		type(locateElement(Locators.XPATH, "//input[@placeholder='Search apps or items...']"), appName);
		click(locateElement(Locators.XPATH,"//p[contains(text(), '" + appNameSubstring +"')]"));
		return new CreateServiceTerritoryPage();
}
	
	public PaymentAuthorizationPage clickPaymentAuthorization() {
		clearAndType(locateElement(Locators.XPATH,"//input[@class='slds-input']"),"Payment Authorization");
		click(locateElement(Locators.XPATH, "//p[@class='slds-truncate']"));
		reportStep("Payment Authorization clicked successfully", "pass");
		return new PaymentAuthorizationPage();
		
	}
}