package com.salesforce.pages;

import com.framework.testng.api.base.ProjectSpecificMethods;

public class PdfDocumentPage extends ProjectSpecificMethods {

	String url="salesforce_summer20_release";
	public PdfDocumentPage verifyUrl() {
	 
		switchToWindow(2);
		verifyUrl(url);
		reportStep("Url Verified","pass");
		return this;
	}
}
