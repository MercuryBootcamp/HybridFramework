package com.salesforce.pages;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class PaymentAuthorizationPage extends ProjectSpecificMethods {
	
	public PaymentAuthorizationPage ClickNewPayment() {

		click(locateElement(Locators.XPATH, "//a[@title='New']"));
		reportStep("Click on New Payment", "pass");
		return this;
	}

	public PaymentAuthorizationPage ClickSave() throws InterruptedException {
		Thread.sleep(4000);
		click(locateElement(Locators.XPATH, "//button[@title='Save']"));
		reportStep("Click on Save", "pass");
		return this;
	}

	public PaymentAuthorizationPage VerifyErrorMessage() {

		String txt=getElementText(locateElement(Locators.XPATH,"//ul/li[text()='These required fields must be completed: Amount, Processing Mode, Status']"));
		System.out.println(txt);
		reportStep("Error Message verfied Successfully", "pass");
		return this;
	}

}
