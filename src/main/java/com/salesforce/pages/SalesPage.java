package com.salesforce.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class SalesPage extends ProjectSpecificMethods {

	public SalesPage ClickSearchCase(String subject) throws InterruptedException {

		click(locateElement(Locators.XPATH, "//input[@placeholder ='Search this list...']"));
		typeAndEnter(locateElement(Locators.XPATH, "//input[@placeholder ='Search this list...']"), "Testing");
		reportStep(" Entered successfully", "pass");
		return this;
	}

	public SalesPage clickDelete() throws InterruptedException {
		click(locateElement(Locators.XPATH, "//a[@title ='Delete']"));
		click(locateElement(Locators.XPATH, "//span[text() ='Delete']"));
		reportStep("Delete button is clicked", "pass");
		return this;

	}

	public SalesPage verifyAfterDeletingCase() {
		String id = getElementText(locateElement(Locators.XPATH, "//span[contains(@class,'toastMessage')]"));
		if (id.contains("deleted.")) {
			System.out.println(id + " and Verified");
		} else {
			System.out.println("Failed to get the text");
		}
		return this;
	}

	public LeadsPage clickLeads() {
		clickUsingJs(locateElement(Locators.XPATH, "//a[@title='Leads']"));
		reportStep(" Leads tab clicked successfully", "pass");
		return new LeadsPage();
	}

	public SalesPage clickMoreOptions() {
		click(Locators.XPATH, "//span[text()='More']/parent::a/lightning-icon");
		reportStep("More options is clicked", "pass");
		return this;
	}

	public SalesPage clickCasesoption() {
		clickUsingJs(locateElement(Locators.XPATH, "//span[text()='Cases']"));
		reportStep("Sales option is clicked", "pass");
		return this;
	}

	public SalesPage clickNew() {
		click(Locators.XPATH, "//div[text()='New']");
		reportStep("New Case is clicked", "pass");
		return this;
	}

	public SalesPage chooseContacts(String ctName) {
		clearAndType(locateElement(Locators.XPATH,
				"//span[text()='Contact Name']/parent::label/following-sibling::div//input"), ctName);
		click(Locators.XPATH, "//div[contains(@title,'" + ctName + "')]");
		reportStep("Contacts Name is chosen", "pass");
		return this;
	}

	public SalesPage setStatus(String status) {
		click(Locators.XPATH, "//label[text()='Status']/following::div//span");
		click(Locators.XPATH, "//span[@title='" + status + "']");
		reportStep("Status entered", "pass");
		return this;
	}

	public SalesPage setCaseOrigin(String caseOrigin) {
		click(Locators.XPATH, "//span[text()='Case Origin']/parent::span//following::a");
		click(Locators.XPATH, "//a[@title='" + caseOrigin + "']");
		reportStep("Origin is set", "pass");
		return this;
	}

	public SalesPage setSubject(String subject) {
		clearAndType(locateElement(Locators.XPATH, "//span[text()='Subject']/parent::label/following::input"), subject);
		reportStep("Subject is entered", "pass");
		return this;
	}

	public SalesPage setDescription(String desc) {
		clearAndType(locateElement(Locators.XPATH, "//span[text()='Description']/parent::label//following::textarea"),
				desc);
		reportStep("Description is entered", "pass");
		return this;
	}

	public SalesPage clickSaveButton() {
		click(locateElement(Locators.XPATH, "//button[@title='Save']"));
		reportStep("Save button is clicked", "pass");
		return this;
	}

	public SalesPage verifyAfterCreatingCase() {
		String id = getElementText(locateElement(Locators.XPATH, "//span[contains(@class,'toastMessage')]"));
		if (id.contains("created.")) {
			System.out.println(id + " and Verified");
		} else {
			System.out.println("Failed to get the text");
		}
		reportStep("Verified After Creating the Case", "pass");
		return this;
	}

	public SalesPage clickMoreMenu() {
		click(Locators.XPATH, "//span[text()='More']");
		reportStep("More Option in Sales page is clicked", "pass");
		return this;
	}

	public SalesPage click3MoreActions() {
		clickUsingJs(locateElement(Locators.XPATH, "//span[text()='Show Actions']/parent::span"));
		reportStep("More Actions button is clicked", "pass");
		return this;
	}

	public SalesPage clickEdit() throws InterruptedException {
		click(Locators.XPATH, "//a[@title ='Edit']");
		reportStep("Edit Case is clicked", "pass");
		return this;
	}

	public SalesPage setPriority(String priority) {
		click(Locators.XPATH, "//span[text()='Priority']/parent::span//following::a");
		click(Locators.XPATH, "//a[@title='" + priority + "']");
		reportStep("Priority is set", "pass");
		return this;
	}

	public SalesPage setSLAViolation(String SLAValue) {
		click(Locators.XPATH, "//span[text()='SLA Violation']/parent::span//following::a");
		click(Locators.XPATH, "//a[@title='" + SLAValue + "']");
		reportStep("SLA Value is set", "pass");
		return this;
	}

	public SalesPage verifyStatusAfterEditing(String status) {
		verifyPartialText(locateElement(Locators.XPATH, "//div/div/table/tbody/tr[1]/td[4]/span/span"), status);
		reportStep("Status value is updated", "pass");
		return this;
	}

}
