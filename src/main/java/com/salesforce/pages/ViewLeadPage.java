package com.salesforce.pages;

import org.openqa.selenium.By;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class ViewLeadPage extends ProjectSpecificMethods {
	
	
	
	public void verifyViewLead()
	{
		
		verifyPartialText(locateElement(Locators.XPATH,"//div[text()='New']/parent::a"),"created lead successful");
		
		
		
		
	}
	
	

}
