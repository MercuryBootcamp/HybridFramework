package com.salesforce.pages;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class EditServiceTerritoryPage extends ProjectSpecificMethods{

	
	public EditServiceTerritoryPage selectRow(String Name) {
		
		typeAndEnter(locateElement(Locators.XPATH, "//input[@placeholder='Search this list...']"), Name);
		return this;
	}
	
	public EditServiceTerritoryPage clickDropDownArrow() throws InterruptedException {
		click(locateElement(Locators.XPATH,"(//div[contains(@class,'forceVirtualAction')])[1]"));
		Thread.sleep(3000);
		return this;
	}
	
	public EditServiceTerritoryPage clickEdit() throws InterruptedException {
		click(locateElement(Locators.XPATH,"//a[@title='Edit']"));
		return this;
	}
	
	public String getCreatedBy() {
		String textCreatedBy = getElementText(locateElement(Locators.XPATH,"(//span[text()='Derrick Dsouza'])[2]"));
		reportStep("Text has been retrieved " + textCreatedBy, "info");
		return textCreatedBy;
	}
	
	public String getModifiedBy() {
		String textLastModifiedBy = getElementText(locateElement(Locators.XPATH, "(//span[text()='Derrick Dsouza'])[3]"));
		reportStep("Text has been retrieved " + textLastModifiedBy, "info");
		return textLastModifiedBy;
	}
	
	public String getOwnerDetails(){
		String textOwner = getElementText(locateElement(Locators.XPATH,"(//span[text()='Derrick Dsouza'])[1]"));
		reportStep("Text has been retrieved " + textOwner, "info"); 
		return textOwner;
	}
	
	public EditServiceTerritoryPage verifyModifiedDetails(String textCreatedBy, String textLastModifiedBy) {
		if(textCreatedBy.contains(textLastModifiedBy))
			System.out.println("Names Matching");
		else
			System.out.println("Names not matching");
		return this;
	}
	
	public EditServiceTerritoryPage changeCountry() throws InterruptedException {
		clearAndType(locateElement(Locators.XPATH,"//input[@placeholder='Country']"),"North America");
		return this;
	}
	
	public EditServiceTerritoryPage clickSaveAfterEdit() {
		click(locateElement(Locators.XPATH,"(//span[text()='Save'])[2]"));
		return this;
	}
	
	public EditServiceTerritoryPage verifyLastModifiedBy() {
		String eleModifiedBy = getElementText(locateElement(Locators.XPATH, "(//span[text()='Last Modified By']/following::div[@class='slds-form-element__control slds-grid itemBody'])[3]"));
		String[] splitModifiedBy = eleModifiedBy.split(",");
		System.out.println("Verify the Last Modified Date" + splitModifiedBy[1]);
		return this;
	}
	
}
