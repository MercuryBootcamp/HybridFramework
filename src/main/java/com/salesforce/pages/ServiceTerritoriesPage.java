package com.salesforce.pages;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class ServiceTerritoriesPage extends ProjectSpecificMethods{
	
	
	public ServiceTerritoriesPage verifyServiceTerritoriesPageTitle() {
		
		String pageTitle = "Recently Viewed | Service Territories | Salesforce";		
		waitForApperance(locateElement(Locators.XPATH, "(//span[text()='Service Territories'])[2]"));
		verifyTitle(pageTitle);
		//verifyExactText(locateElement(Locators.XPATH, "(//span[text()='Service Territories'])[2]"),pageTitle);
		return this;
	}
	
	public ServiceTerritoriesPage clickNewButton() {		
		click(locateElement(Locators.XPATH, "//a[@title='New']"));
		reportStep("Clicked the New Button", "pass");
		return this;
	}
	
	public ServiceTerritoriesPage enterNameInNewServiceTerritory(String name) {
		clearAndType(locateElement(Locators.XPATH, "//input[@class=' input']"),name);
		reportStep("Entered the name in NewServiceTerritory", "pass");
		return this;
	}
	
	public ServiceTerritoriesPage enterOperatingHoursInNewServiceTerritory() {
		click(locateElement(Locators.XPATH, "//input[@title='Search Operating Hours']"));
		click(locateElement(Locators.XPATH, "//div[text()='US Shift']"));
		click(locateElement(Locators.XPATH, "//button[@title='Save']"));		
		reportStep("Selected the Shift in Dropdown", "pass");
		return this;
	}
	
	public ServiceTerritoriesPage verifyTheToastMessage() {
		String value = "\"David\"";
		String ActualText = "Service Territory " + value + " was created.";	
		waitForApperance(locateElement(Locators.XPATH, "//span[@data-aura-class='forceActionsText']"));
		verifyExactText(locateElement(Locators.XPATH, "//span[@data-aura-class='forceActionsText']"),ActualText);
		reportStep("Verified the Created NewServiceTerritory", "pass");
		return this;
	}
	
}

