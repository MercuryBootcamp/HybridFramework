package com.salesforce.pages;

import com.framework.selenium.api.design.Locators;
import com.framework.testng.api.base.ProjectSpecificMethods;

public class LeadsPage extends ProjectSpecificMethods {

	public NewLeadPage clickNewButton() {

		click(locateElement(Locators.XPATH, "//div[text()='New']"));
		reportStep(" Sales link clicked successfully", "pass");
		return new NewLeadPage();
	}


}