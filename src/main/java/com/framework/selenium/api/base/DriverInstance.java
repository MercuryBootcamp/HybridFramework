package com.framework.selenium.api.base;


import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverInstance  {

	private static final ThreadLocal<RemoteWebDriver> remoteWebdriver = new ThreadLocal<RemoteWebDriver>();
//	private static final ThreadLocal<WebDriver> remoteWebdriver = new ThreadLocal<WebDriver>();
	private static final ThreadLocal<WebDriverWait> wait = new  ThreadLocal<WebDriverWait>();
	public  DesiredCapabilities dc;
	


public void setWait() throws MalformedURLException {
		wait.set(new WebDriverWait(getDriver(), 30));
	}
	
	public WebDriverWait getWait() {
		return wait.get();
	}

	public void setDriver(String browser, boolean headless,String mode) throws MalformedURLException {	
	
		
		  dc=new DesiredCapabilities(); dc.setPlatform(Platform.WINDOWS);
		  dc.setBrowserName("chrome");
		 
		switch (browser) {
		case "chrome":
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized"); 
			options.addArguments("--disable-notifications"); 
			options.addArguments("--incognito");
			if(mode.equals("sauce"))
			{
			RemoteWebDriver driver=new RemoteWebDriver(new URL("https://veera23:8d54394b-24fc-4031-8634-f2703a78c0c7@ondemand.apac-southeast-1.saucelabs.com:443/wd/hub"), dc);
			remoteWebdriver.set(driver);
			}
			else
			{
				remoteWebdriver.set(new ChromeDriver(options));
		}
			
			//
			break;
		case "firefox":
			remoteWebdriver.set(new FirefoxDriver());
			break;
		case "ie":
			remoteWebdriver.set(new InternetExplorerDriver());
		default:
			break;
		}
	}
	public RemoteWebDriver getDriver()  {
		return remoteWebdriver.get();
		
		
		
	}

	

	
	
}
